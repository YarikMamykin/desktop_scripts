#! /bin/bash

SCREENSHOTS_DIR="${HOME}/Pictures/screenshots"
mkdir -p "${SCREENSHOTS_DIR}"
number=$(ls "${SCREENSHOTS_DIR}" | wc -l)
number=$((number + 1))

if [ -z "$1" ]; then
  $(which import) -window root ${SCREENSHOTS_DIR}/screenshot_${number}.png
else
  $(which import) ${SCREENSHOTS_DIR}/screenshot_${number}.png
fi


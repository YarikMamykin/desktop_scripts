#! /bin/bash -vex

##########################################
# DWM Status bar configuration
##########################################


STATUS_BAR_ALREADY_LAUNCHED="$(ps aux | grep status_bar.sh | grep -ve grep | grep -ve vim | wc -l)"
STATUS_BAR_ALREADY_LAUNCHED=$((STATUS_BAR_ALREADY_LAUNCHED - 1))
STATE_FILE="/bin/.dwm/.current_status_bar_state"

# STATES
OFF=0
LAUNCHED_FULL=1
LAUNCHED_TIME_ONLY=2
LAUNCHED_SYSTEM_STATE=3
LAUNCHED_SOUND=4

function stop_status_update {
  xsetroot -name $(echo -e "=\uf011=")
  killall $(basename $0)
}

function restart_status_update {
  for process in "$(ps aux | grep status_bar.sh | grep -ve grep | grep -ve vim)"; do 
    process=$(echo $process | awk '{ print $2 }')
    if [ $process -ne $$ ]; then
      kill -9 $process
    fi
  done
}

function change_state {
  if ! [ -f "$STATE_FILE" ]; then
    STATE=$OFF
  else
    STATE=$(head "$STATE_FILE")
  fi

  restart_status_update;

  case $STATE in
    $OFF) STATE=$LAUNCHED_FULL;;
    $LAUNCHED_FULL) STATE=$LAUNCHED_TIME_ONLY;;
    $LAUNCHED_TIME_ONLY) STATE=$LAUNCHED_SYSTEM_STATE;;
    $LAUNCHED_SYSTEM_STATE) STATE=$LAUNCHED_SOUND;;
    $LAUNCHED_SOUND) STATE=$OFF; 
  esac

  echo $STATE > $STATE_FILE
}

if   [ "$1" == "-s" ]; then
  STATUS_SHORT="YES"
elif [ "$1" == "-d" ]; then
  STATE=$OFF
  echo $STATE > $STATE_FILE
  stop_status_update
elif [ "$1" == "-t" ]; then
  change_state
else 
  if [ $STATUS_BAR_ALREADY_LAUNCHED -gt 1 ]; then
    exit 0;
  else 
    if ! [ -f "$STATE_FILE" ]; then
      STATE=$LAUNCHED_FULL
    else
      STATE=$(head "$STATE_FILE")
    fi
  fi
fi


function off {
  echo -e "=\uf011="
}

function player_status {
  local query_file="/bin/.dwm/cmus_query_file"
  local cmus_query=$(cmus-remote -Q 2>&1 > $query_file)
  local this_status=''
  if [[ "$cmus_query" == *"not running"* ]]; then 
    this_status="--\uf04d--"
  else
    # Artist and title
    local player_current_artist=$(grep -E "tag artist" "$query_file" | awk '{print $3 $4 $5}')
    local player_current_song=$(grep -E "tag title" "$query_file" | awk '{print $3 $4 $5}')
    this_status="$this_status $player_current_artist ^ $player_current_song"

    # Playing status
    local player_status=$(grep -E "status" $query_file | awk '{print $2}')
    case "$player_status" in
      "playing") this_status="$this_status \uf04b";;
      "paused") this_status="$this_status \uf04c";;
      "stopped") this_status="$this_status \uf04d";;
    esac

    # Volume level
    local player_vol=$(grep -E "set vol_left" $query_file | awk '{print $3}') # assuming left/right volumes equal
    case $player_vol in
      0) this_status="$this_status \uf026 $player_vol%";;
      *) this_status="$this_status \ufa7d $player_vol%";;
    esac
  fi

  echo -e "$this_status"
}

function upt {
  local time_from_start=$(echo $(uptime) \
    | sed 's/^.* up\(.*\)/\1/g' \
    | sed 's/^\(.*min,\).*/\1/g' \
    | sed 's/^ *//g' \
    | awk '{print $1 $2 $3}' \
    | sed 's/,/ /g' \
    | sed 's/min/m/g' \
    | sed 's/day/d/g' \
    | sed 's/:\(.*\)/h \1m/g' \
    | sed 's/ *$//g' \
    | sed 's/[[:digit:]]user[s]*//g' \
    | sed 's/\([[:digit:]]\) *m/\1m/g')
  echo -e "\ufc35 $time_from_start"
}

function dte {
  local date_time="\ufc8a W$(date +%U) $(date +'%a %d-%m-%y %R')"
  echo -e "$date_time"
}

function sdte {
  local date_time="\ufc8a $(date +'%d-%m %R')"
  echo -e "$date_time"
}

function mem {
  local mem_stat="$(free | awk '/Mem/ {printf "%d/%d\n", $3 / 1024.0, $2 / 1024.0 }')"
  local actual_in_use="${mem_stat%%\/*} MB"
  echo -e "\uf2db $(printf "%.2f" $(echo "$mem_stat * 100" | bc -l))% ($actual_in_use)"
}

function cpu {
  read cpu a b c previdle rest < /proc/stat
  prevtotal=$((a+b+c+previdle))
  sleep 0.5
  read cpu a b c idle rest < /proc/stat
  total=$((a+b+c+idle))
  cpu=$((100*( (total-prevtotal) - (idle-previdle) ) / (total-prevtotal) ))
  echo -e "\uf109 ${cpu}%"
}

VOL_SOURCE_HEADPHONES_MARK="alsa_output.*USB_Audio_Device.*"

function vol {
  local vol_mute="$(pamixer --get-mute)"
  local vol_source="$(pamixer --list-sources | grep ${VOL_SOURCE_HEADPHONES_MARK})"
  
  if [ -n "$vol_source" ]; then
    vol_source="\uf025"
  else 
    vol_source="\ufac4"
  fi

  if [ "$vol_mute" = "true" ]; then
    echo -e "${vol_source} \uf466"
  else
    local vol_lvl="$(pamixer --get-volume)"
    if [ $vol_lvl -gt 66 ]; then
      echo -e "${vol_source} \uf028 ${vol_lvl}%"
    elif [ $vol_lvl -gt 33 ] && [ $vol_lvl -lt 67 ]; then
      echo -e "${vol_source} \ufa7d ${vol_lvl}%"
    elif [ $vol_lvl -gt 0 ] && [ $vol_lvl -lt 33 ]; then
      echo -e "${vol_source} \ufa7f ${vol_lvl}%"
    else
      echo -e "${vol_source} \uf026 ${vol_lvl}%"
    fi
  fi
}

function mic {
  local device_id="$(pamixer --list-sources | grep alsa_input)"; device_id="${device_id%% *}"
  local mic_mute="$(pamixer --source ${device_id} --get-mute)"
  if [ "$mic_mute" = "true" ]; then
    echo -e " \uf131"
  else
    local mic_vol="$(pamixer --source ${device_id} --get-volume)"
    if [ -n "$mic_vol" ]; then
      echo -e " \uf130 ${mic_vol}%"
    else
      echo -e ""
    fi
  fi
}

function bat {
    local charge="$(cat /sys/class/power_supply/BAT0/capacity)"
    local bat_state="$(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep state)"
    bat_state="${bat_state##* }"

    if [ $charge -gt 95 ]; then
      echo -e "\uf578 ${charge}%"
    elif [ $charge -gt 90 ] && [ $charge -lt 96 ]; then
      if [ "$bat_state" == "charging" ]; then
        echo -e "\uf58a ${charge}%"
      else 
        echo -e "\uf581 ${charge}%"
      fi
    elif [ $charge -gt 80 ] && [ $charge -lt 91 ]; then
      if [ "$bat_state" == "charging" ]; then
        echo -e "\uf589 ${charge}%"
      else 
        echo -e "\uf580 ${charge}%"
      fi
    elif [ $charge -gt 60 ] && [ $charge -lt 81 ]; then
      if [ "$bat_state" == "charging" ]; then
        echo -e "\uf588 ${charge}%"
      else 
        echo -e "\uf57f ${charge}%"
      fi
    elif [ $charge -gt 40 ] && [ $charge -lt 61 ]; then
      if [ "$bat_state" == "charging" ]; then
        echo -e "\uf587 ${charge}%"
      else 
        echo -e "\uf57c ${charge}%"
      fi
    elif [ $charge -gt 20 ] && [ $charge -lt 41 ]; then
      if [ "$bat_state" == "charging" ]; then
        echo -e "\uf586 ${charge}%"
      else 
        echo -e "\uf57b ${charge}%"
      fi
    elif [ $charge -gt 0 ] && [ $charge -lt 21 ]; then
      if [ "$bat_state" == "charging" ]; then
        echo -e "\uf585 ${charge}%"
      else
        echo -e "\uf57a ${charge}%"
      fi
    else
      echo -e "\uf582 ${charge}%"
    fi
}

function lan {
  pushd $PWD &>/dev/null
    cd $(dirname $0)/xkb-switch/ 
    language="$(xkb-switch)"
  popd &>/dev/null

  echo -e "\uf812 ${language}"
}

function con {
  local con_string=''
  if ping -c 1 8.8.8.8 &>/dev/null; then
    # echo -e "\ufaa8" # in case of wifi
    con_string="\uf700" 
  else
    # echo -e "\ufaa9" # in case of wifi
    con_string="\uf701" 
  fi

  con_string="$con_string ($(iwconfig 2>/dev/null | grep -o -E "ESSID:.*" | sed 's/ESSID:\(.*\)/\1/' | sed 's/"\(.*\)"/\1/' | awk '{$1=$1;print}'))"
  echo -e "$con_string"
}

function blz {
  local bluez_dev_count=$(hcitool dev | wc -l)
  if [ $bluez_dev_count -eq 1 ]; then
    echo -e "\uf5b1"
  else
    echo -e "\uf5b0"
  fi
}

function pc_status {
  case $STATE in
    $OFF) xsetroot -name "$(off)";;
    $LAUNCHED_FULL) xsetroot -name "${EDGE}$(con) $(blz) $(upt)${SEP}$(cpu)${SEP}$(mem)${SEP}$(player_status)${SEP}$(vol)$(mic)${SEP}$(lan)${SEP}$(dte)${EDGE}";;
    $LAUNCHED_TIME_ONLY) xsetroot -name " $(upt)${SEP}$(dte) ";;
    $LAUNCHED_SYSTEM_STATE) xsetroot -name "${EDGE}$(con) $(blz)${SEP}$(cpu)${SEP}$(mem)${SEP}$(lan)${EDGE}";;
    $LAUNCHED_SOUND) xsetroot -name "$(player_status)${SEP}$(vol)$(mic)${EDGE}";;
  esac
}

function laptop_status {
  case $STATE in
    $OFF) xsetroot -name "$(off)";;
    $LAUNCHED_FULL) xsetroot -name "${EDGE}$(con) $(blz)${SEP}$(cpu)${SEP}$(mem)${SEP}$(player_status)${SEP}$(vol)$(mic)${SEP}$(bat)${SEP}$(lan)${SEP}$(dte) $(upt)${EDGE}";;
    $LAUNCHED_TIME_ONLY) xsetroot -name " $(upt)${SEP}$(dte) ";;
    $LAUNCHED_SYSTEM_STATE) xsetroot -name "${EDGE}$(con) $(blz)${SEP}$(cpu)${SEP}$(mem)${SEP}$(bat)${SEP}$(lan)${EDGE}";;
    $LAUNCHED_SOUND) xsetroot -name "$(player_status)${SEP}$(vol)$(mic)${EDGE}";;
  esac
}

SEP="  " 
EDGE="  "
FEDGE=" "
while true; do
  # pc_status
	laptop_status
done & 

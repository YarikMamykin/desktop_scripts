#! /bin/bash 

# # It is better to set primary screen as first from left side.
# # Other screens should be one by one accordingly to their sequence from left to right
# SCREEN1=HDMI-1
# SCREEN2=eDP-1

# # set main screen
# xrandr --output ${SCREEN2} --auto --mode 1920x1080 --primary
# # set side screen
# xrandr --output ${SCREEN1} --auto --same-as ${SCREEN2}

#!/bin/bash

if [[ -z "$1" ]]; then 
	exit 1; 
fi;

FILE="$1"

cat $FILE | xclip -sel clip -target image/png -i

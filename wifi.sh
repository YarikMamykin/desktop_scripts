#! /bin/bash

if [ $# -eq 0 ]; then 
  exit
fi

function connect {
  nmcli device wifi connect "$SSID" password "$PASS"
}

function disconnect {
  nmcli device disconnect ifname "$INTERFACE"
}

function list {
  nmcli device wifi list
}

while true; do 
  case "$1" in
    "-iface") shift; INTERFACE="$1"; disconnect; break;;
    "-list") shift; list; break;;
    "-pass") shift; PASS="$1";;
    "-ssid") shift; SSID="$1";;
  esac

  if [ -n "$SSID" ] && [ -n "$PASS" ]; then
    connect
    break
  fi

  shift
done


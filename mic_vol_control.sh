#! /bin/bash

function get_device_id {
  local device_id="$(pamixer --list-sources | grep alsa_input)"; 
  echo "${device_id%% *}"
}

function increase_mic_volume {
  /bin/pamixer --source $(get_device_id) -i $1 
}

function decrease_mic_volume {
  /bin/pamixer --source $(get_device_id) -d $1 
}

function toggle_mic_mute {
  /bin/pamixer --source $(get_device_id) -t 
}

case $1 in
  "-i") shift; increase_mic_volume $1  ;;
  "-d") shift; decrease_mic_volume $1  ;;
  "-t") toggle_mic_mute ;;
esac

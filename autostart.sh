#! /bin/bash 

source ${PWD}/status_bar.sh 
source ${PWD}/setup_displays.sh

##########################################
# Additional system config
##########################################

# set keyboard layouts
setxkbmap -layout us,ru,ua 

# setr background
feh --bg-fill background.jpg

# remove dimming
xset -dpms
xset s off

# terminal transparency
xcompmgr &

tmux new-session -d -s startup_session &

#! /bin/bash -vex

CONFIG="${HOME}/.config/alacritty.yml"
OPACITY=$(cat ${CONFIG} | sed -e "s/\\(background_opacity: \\)\\(\\d\\.\\d\\)/\\2/p" | grep "background_opacity")
OPACITY=${OPACITY##* }
OPACITY=$(echo "${OPACITY}$1" | bc | sed 's/-/0/g')
[ $(echo "${OPACITY} > 1" | bc) -ne 1 ] && alacritty -e sed -i "s/\\(background_opacity: \\).*/\\1${OPACITY}/g" ${CONFIG}

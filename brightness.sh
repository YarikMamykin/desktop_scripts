#! /bin/bash 

BRIGHTNESS_DIRECTION="$1"
BRIGHTNESS_LEVEL="$2"
CURRENT_BRIGHTNESS_LEVEL=$(xrandr --current --verbose | grep -m1 Brightness)
CURRENT_BRIGHTNESS_LEVEL=${CURRENT_BRIGHTNESS_LEVEL##* }
WANTED_BRIGHTNESS_LEVEL=$(echo "$CURRENT_BRIGHTNESS_LEVEL $BRIGHTNESS_DIRECTION $BRIGHTNESS_LEVEL" | bc -l | sed -e 's/^\./0./' -e 's/^-\./-0./')

if [ $(echo "$WANTED_BRIGHTNESS_LEVEL > 1" | bc -l) -eq 1 ]; then
  WANTED_BRIGHTNESS_LEVEL=1
elif [ $(echo "$WANTED_BRIGHTNESS_LEVEL < 0" | bc -l) -eq 1 ]; then
  WANTED_BRIGHTNESS_LEVEL=0
fi


IFS=$'\n'
SCREENS=()
for e in $(xrandr | grep " connected"); do
  e=$(echo ${e} | cut -f1 -d " ")
  SCREENS+=("${e}")
done
unset IFS

for e in "${SCREENS[@]}"; do
  xrandr --output ${e} --brightness $WANTED_BRIGHTNESS_LEVEL
done
